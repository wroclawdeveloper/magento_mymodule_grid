<?php
/** @var Mage_Core_Model_Resource_Setup $installer */
//$installer = $this;
//$installer->startSetup();
//
//$table = $installer->getConnection()
//    ->newTable($this->getTable('example/report'))
//    ->addColumn('id',Varien_Db_Ddl_Table::TYPE_BIGINT,null,array(
//        'identity' => true,
//        'unsigned' => true,
//        'nullable' => false,
//        'primary'  => true
//    ))
//    ->addColumn('ip',Varien_Db_Ddl_Table::TYPE_TEXT,null,array(
//        'length'    => 15,
//        'nullable' => false
//    ))
//    ->addColumn('timestamp',Varien_Db_Ddl_Table::TYPE_TIMESTAMP,null,array(
//        'nullable' => false,
//        'default'  => Varien_Db_Ddl_Table::TIMESTAMP_INIT
//    ))
//    ->addColumn('product_id',Varien_Db_Ddl_Table::TYPE_INTEGER,null,array(
//        'nullable' => false
//    ));
//$installer->getConnection()->createTable($table);
//
//$installer->endSetup();



$installer = $this;
$installer->startSetup();
$sql=<<<SQLTEXT
CREATE TABLE IF NOT EXISTS `mycompany_report` (
        `id` bigint(20) NOT NULL AUTO_INCREMENT,
        `ip` varchar(15) NOT NULL,
        `product_id` int(13) NOT NULL,
        `timestamp` datetime DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
SQLTEXT;

$installer->run($sql);
$installer->endSetup();
	 