<?php
$installer = $this;
$installer->startSetup();
if ($installer->tableExists($installer->getTable('example/report'))) {
    $installer->getConnection()
        ->addIndex(
            $installer->getTable('example/report'),
            $installer->getIdxName('example/report', array('ip', 'product_id'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
            array('ip', 'product_id'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        );
}
$installer->endSetup();