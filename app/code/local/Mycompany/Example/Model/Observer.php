<?php
class Mycompany_Example_Model_Observer extends Varien_Event_Observer
{
    public function __construct()
    {

    }

    /**
     * @param Varien_Event_Observer $observer
     * @return Varien_Event_Observer
     */
    public function getCustomerData(Varien_Event_Observer $observer)
    {
        $pModel = Mage::getModel('example/report');

        $product = Mage::registry('current_product');
        $id = $product->getId();
        $ip = Mage::helper('core/http')->getRemoteAddr();

        $pModel->setData(['ip' => $ip, 'product_id' => $id])->save();

        return $observer;
    }
}
